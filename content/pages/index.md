title: index page
date: 2021-01-08
modified: 2022-02-09 17:47:12
URL: 
category: 
tags: 
authors: 12 Volt Farmer
summary: the index page summary
status: hidden

This is the text of the index page.

This is the second line of the index page.

Apparently required according to [Pelican FAQ page](http://docs.getpelican.com/en/3.6.3/faq.html#how-can-i-use-a-static-page-as-my-home-page)

These two header tags moved into body pending initial testing because:
- 'Template: index_page' tag breaks the pelican build until the template is created and
- 'save_as: index.html' affects the default build


