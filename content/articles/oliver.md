title: Oliver 
date: 2020-12-23 11:41
modified: 2022-02-15 13:15:40
category: 
tags: 
slug: 
authors: 12 Volt Farmer
summary: Tests config and videos for video rendering and PWA caching.
image: images/pic05.jpg

Video and image styling tests:

1. Rendering
2. PWA Caching

- Link to [videos/oliver2avi.mp4](videos/oliver2avi.mp4)

- Link to [videos/opt_oliver2avi.mp4](videos/opt_oliver2avi.mp4)

<video controls="controls">
    <source src="videos/oliver2avi.mp4" type="video/mp4">
    Your browser does not support the HTML5 Video element.
</video>
[caption]oliver2avi.mp4</p>


<video controls="controls">
    <source src="videos/opt_oliver2avi.mp4" type="video/mp4">
    Your browser does not support the HTML5 Video element.
</video>
[caption] opt_oliver2avi.mp4

