title: Creating Pelican Themes
date: 2021-05-29 16:06
modified: 2022-02-10 21:38:22
category: 
tags: History
slug: 
authors: 12 Volt Farmer
summary: A collection of links about creating pelican themes
image: images/pic01.jpg

Notes on Pelican themes that look best suited for mobile use:

##### Best looking:

- [Kirill Savine's version of Twenty](https://github.com/kirillsavine/twenty-pelican-html5up)

Needs skel.js and the css in the web server's root directory, not themes/ directory. Probably achievable with a pre-build script in pull_repo....sh

- [pelican-html5up-spectral](https://github.com/abeelen/pelican-html5up-spectral) Needs css, js, and poss fonts moving into root (possible skel?)

- [nonameentername's HTML5up future-imperfect theme](https://github.com/nonameentername/html5up-future-imperfect)

##### Specific purpose (kids' sites or dev sites):

- [Eveee](https://kura.gg/eevee/#features)

- [Materialistic Pelican](https://github.com/eswarm/materialistic-pelican)

- [Pelican-Material](https://github.com/greizgh/pelican-material)

- [Pelican-Materialize](https://github.com/z/doknowevil.net/tree/master/site/themes/pelican-materialize-blog) see example [here](http://www.doknowevil.net/) Nice flyout



##### Various:

[Jeff Mackinnon 1](https://jeffmackinnon.com/how-install-pelican-and-get-started.html)
[Jeff Mackinnon's Editorial theme](https://jeffmackinnon.com/i-built-a-pelican-theme.html)
[Jeff Mackinnon plugins and why](https://jeffmackinnon.com/pages/my-pelican-ssg-setup.html)

These need some articles_page setting.

[Pelican HTML5up Halcyonic](https://github.com/drakonstudio/pelican-html5up-halcyonic) Needs 'ordinal' filter for date handling.

[Hypotensive](https://github.com/type1joe/hypotensive-theme)

##### Various Twenty themes:

[https://github.com/kirillsavine/twenty-pelican-html5up](https://github.com/kirillsavine/twenty-pelican-html5up)

[https://github.com/frankV/twenty-pelican-html5up](https://github.com/frankV/twenty-pelican-html5up)

[https://gitlab.science.ru.nl/dvdbrink/twenty-pelican-html5up](https://gitlab.science.ru.nl/dvdbrink/twenty-pelican-html5up)

[The Digital Cat Online](https://www.thedigitalcatonline.com/blog/2021/03/25/how-to-write-a-pelican-theme-for-your-static-website/)

[Writing themes](https://bblais.github.io/posts/2020/Jul/03/something-satisfying-about-building-pelican-themes/)

[Convert pelican blog to PWA](https://cloudbytes.dev/snippets/convert-a-pelican-website-to-pwa-using-workbox)

[Making a Simple PWA](https://www.geeksforgeeks.org/making-a-simple-pwa-under-5-minutes/)

[Another on making a PWA](https://tudip.com/blog-post/how-to-turn-a-website-or-web-application-into-pwa-with-example/)

