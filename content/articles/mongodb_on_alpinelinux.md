title: Installing mongodb on Alpine Linux >= 3.11
date: 2020-05-04 21:39
modified: Wed 17 Jun 2020 15:22:40 UTC
category: 
tags: Tech
slug: 
authors: 12 Volt Farmer
summary: Compiled from SO and reading around
status: published
image: images/pic02.jpg

Steps are captured in this script:

```bash
echo 'http://dl-cdn.alpinelinux.org/alpine/v3.9/main' >> /etc/apk/repositories
echo 'http://dl-cdn.alpinelinux.org/alpine/v3.9/community' >> /etc/apk/repositories
apk update
apk add mongodb yaml-cpp=0.6.2-r2
apk add mongodb-tools  # to add mongodump and mongorestore
rc-update add mongodb default
```

Thanks to [valiano](https://unix.stackexchange.com/users/246490/valiano) for [https://unix.stackexchange.com/questions/568530/installing-mongodb-on-alpine-3-9](https://unix.stackexchange.com/questions/568530/installing-mongodb-on-alpine-3-9)

Check start up time after loading with data. May be worth plotting time from start to port 27017 (or whatever) coming up to be sure service availability testing is in line with start up and data-load latency.

### Alternative Method

Compile from source per: [https://unix.stackexchange.com/questions/568530/installing-mongodb-on-alpine-3-9]('https://unix.stackexchange.com/questions/568530/installing-mongodb-on-alpine-3-9)

But looks to have a failed libresolv.so.2 dependency.
