title: Myth as Teaching Device
date: 2019-07-02 11:27
modified: Wed 17 Jun 2020 15:14:08 UTC
category: 
tags: History
slug: 
authors: 12 Volt Farmer
summary: Placeholder for links to pieces on myth as a system for encoding knowledge
status: published
image: images/pic04.jpg

[Ouroborous as orbit](http://www.mythopedia.info/vanderSluijs-CC13.pdf) by van der Sluis

[The Battle of the Wizards](http://levigilant.com/Bulfinch_Mythology/bulfinch.englishatheist.org/olcott/Olcott.htm#bw05)

[http://levigilant.com/Bulfinch_Mythology/bulfinch.englishatheist.org/index.html](Various books of myths and tales)

[31 WIRREENUN THE RAINMAKER NATIVE TEXT OF THE FIRST TALE](http://levigilant.com/Bulfinch_Mythology/bulfinch.englishatheist.org/oz/OzLand.html#a31)
