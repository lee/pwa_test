title: North Pole
date: 2019-07-02 13:24
modified: Wed 17 Jun 2020 15:24:22 UTC
category: 
tags: History
slug: 
authors: 12 Volt Farmer
summary: Links to articles on locations and movement paths of the magnetic north pole
status: published
image: images/pic05.jpg
D3:
Scripts: d3_vis_1.js, d3_vis_2.js
Styles: d3_styles.css

[Malaga Bay shows the North Pole in Greenland](https://malagabay.wordpress.com/2019/07/01/alaskan-muck-the-swing-state/)

[MarioBuildReps shows how the North Pole moved through Greenland](https://mariobuildreps.com/orientation-pyramids-former-north-poles/)
