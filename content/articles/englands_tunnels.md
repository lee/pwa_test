title: England's Tunnels
date: 2022-02-11 10:56
modified: 2022-02-15 13:35:52
category: 
tags: History
slug: 
authors: 12 Volt Farmer
summary: Links collected for people interested in Caroline Coram's Request for Information (RFI) about tunnels beneath English towns and villages.
image: images/pic03.jpg

[Caroline Coram asked about tunnels beneath English towns...](https://twitter.com/CarolineCoramUK/status/1492044061364174850)

<video controls="controls" onclick=playPause()>
    <source src="videos/opt_clip_You_Cant_Handle_the_Truth_1992.mp4" type="video/mp4">
    Your browser does not support the HTML5 Video element. Try: [https://test.12voltfarm.com/videos/opt_clip_You_Cant_Handle_the_Truth_1992.mp4](https://test.12voltfarm.com/videos/opt_clip_You_Cant_Handle_the_Truth_1992.mp4)
</video>
[caption] You can't handle the truth! Source: [A Few Good Men (1992)](https://www.imdb.com/title/tt0104257/)

Although, at one time, the English public knew who built them and why.

Five clues:

1. [Hybridisation specialist Eugene McCarthy conjectures that humans are hybrids.](http://www.macroevolution.net/human-hybrids.html)

2. [Belgian, Dutch](https://paulbuddehistory.com/europe/the-great-death/) and [German](https://frisiacoasttrail.blog/2020/10/14/half-a-million-deaths-a-forgotten-north-sea-disaster/) histories all record vast North Sea floods, sometimes followed by multi-year famines. But these floods are missing from eastern England's history. Even though Dutch accounts sometimes claim English fatalities were worse.

3. Higher level military know who built the tunnels and why. Eg, carefully read [Gainsborough photographers discussing the town's tunnels](https://www.flickr.com/groups/689281@N23/discuss/72157623539758694/).

4. Some mining geologists know. [Did someone quarry Earth's wilderness?](https://ln.nomagic.uk/engquarriedearth) (ignore the ssl warning - people interested in secrets don't let their browser tell them what they can read.)

5. The 'Church' knows who built the tunnels and why. England's churches, monasteries, abbeys, friaries and priories changed hands twice. Both changes were accompanied by large-scale destruction of 'religious' buildings:
    - The nine-year 1536-1545 Dissolution of the Monasteries.
    - The nine-year 1646-1652 English Civil War.

<!--
Others are turning them. Read '[IHASFEMR](https://stolenhistory.net/threads/evidence-humans-were-created-and-traded-as-slaves-food-entertainment-and-material-resources-ihasfemr.5379/)':
> IHASFEMR is memorably close to 'I Has Femur' - femurs apparently having been a popular product SKU - judging by the quantities of them warehoused in church crypts...
-->
